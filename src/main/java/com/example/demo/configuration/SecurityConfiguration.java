package com.example.demo.configuration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private BCryptPasswordEncoder encoder;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication().withUser("admin").password(passwordEncoder().encode("777")).roles("ADMIN");
        auth.inMemoryAuthentication().withUser("user").password(passwordEncoder().encode("777")).roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/book").permitAll().antMatchers(HttpMethod.GET,"/category").permitAll()
                .antMatchers( HttpMethod.POST,"/category/**").hasRole("ADMIN").antMatchers( HttpMethod.POST,"/book/**").hasRole("ADMIN")
                .antMatchers( HttpMethod.DELETE,"/category/**").hasRole("ADMIN").antMatchers( HttpMethod.DELETE,"/book/**").hasRole("ADMIN")
                .antMatchers( HttpMethod.PUT,"/category/**").hasRole("ADMIN").antMatchers( HttpMethod.PUT,"/book/**").hasRole("ADMIN")
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }
    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return new BCryptPasswordEncoder();
    }
}
