package com.example.demo.controller;

import com.example.demo.controller.request.CategoryRequestModel;
import com.example.demo.controller.respone.BaseApiRespone;
import com.example.demo.repository.model.BookModel;
import com.example.demo.repository.model.categoryModel;
import com.example.demo.service.bookImpement.BookImpService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class BookRestController {
    private BookImpService bookImpService;
    @Autowired
    public BookRestController(BookImpService bookImpService) {
        this.bookImpService = bookImpService;
    }
    @PostMapping("/book")
    public ResponseEntity<BaseApiRespone<BookModel>> insert(
            @RequestBody BookModel bookModel) {
        BaseApiRespone<BookModel> response = new BaseApiRespone<>();
        ModelMapper mapper = new ModelMapper();
        bookImpService.insert(bookModel);
        response.setMessage("added successfully");
        response.setStatus(HttpStatus.CREATED);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/book")
    public ResponseEntity<BaseApiRespone<List<BookModel>>> select() {

        ModelMapper mapper = new ModelMapper();
        BaseApiRespone<List<BookModel>> response = new BaseApiRespone<>();

        List<BookModel> bookList = bookImpService.select();
        response.setMessage("all Books successfully");
        response.setData(bookList);
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/book/{id}")
    public ResponseEntity<BaseApiRespone<BookModel>> findOne(@PathVariable int id) {

        ModelMapper mapper = new ModelMapper();
        BaseApiRespone<BookModel> response = new BaseApiRespone<>();
        BookModel resutl=bookImpService.findOne(id);
        response.setMessage("This Book");
        response.setData(resutl);
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/book/{id}")
    public ResponseEntity<BaseApiRespone<String>> delete(@PathVariable int id)
    {
        BaseApiRespone<String> response = new BaseApiRespone<>();
        bookImpService.delete(id);
        response.setMessage("Delete successfully");
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
    @GetMapping("/book/search")
    public ResponseEntity<BaseApiRespone<List<BookModel>>> selectByFilter(
            @RequestParam(required = false) String title,@RequestParam(required = false,defaultValue ="") String category) {

        ModelMapper mapper = new ModelMapper();
        List<BookModel> bookTitle;
        BaseApiRespone<List<BookModel>> response = new BaseApiRespone<>();
        if(!category.isEmpty())
        {
            bookTitle = bookImpService.selectByFilters(title,Integer.parseInt(category));
        }else
        {
            bookTitle = bookImpService.selectByFilter(title);
        }
        if(bookTitle.isEmpty()){
            response.setMessage("No Book By title");
            response.setData(bookTitle);
            response.setStatus(HttpStatus.OK);
        }
        else
        {
            response.setMessage("Find Book By title");
            response.setData(bookTitle);
            response.setStatus(HttpStatus.OK);
        }
        return ResponseEntity.ok(response);
    }
    @PutMapping("/book/{id}")
    public ResponseEntity<BaseApiRespone<String>> update(@RequestBody BookModel bookModel,
                                                         @PathVariable("id") Integer id)
    {
        BaseApiRespone<String> response = new BaseApiRespone<>();
        bookImpService.update(bookModel,id);
        response.setMessage("update successfully");
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
}
