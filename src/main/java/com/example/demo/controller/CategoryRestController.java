package com.example.demo.controller;

import com.example.demo.controller.request.CategoryRequestModel;
import com.example.demo.controller.respone.BaseApiRespone;
import com.example.demo.repository.model.categoryModel;
import com.example.demo.service.categoryImplement.CategoryImpService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
@RestController
public class CategoryRestController {
    private CategoryImpService categoryImpService;
    @Autowired
    public CategoryRestController(CategoryImpService categoryImpService) {
        this.categoryImpService = categoryImpService;
    }
    @PostMapping("/category")
    public ResponseEntity<BaseApiRespone<CategoryRequestModel>> insert(
            @RequestBody CategoryRequestModel categoryRequestModel) {

        BaseApiRespone<CategoryRequestModel> response = new BaseApiRespone<>();
        ModelMapper mapper = new ModelMapper();
        categoryModel cateogry = mapper.map(categoryRequestModel, categoryModel.class);

        categoryModel result = categoryImpService.insert(cateogry);

        CategoryRequestModel result2 = mapper.map(result, CategoryRequestModel.class);

        response.setData(result2);
        response.setMessage("added successfully");
        response.setStatus(HttpStatus.CREATED);
        return ResponseEntity.ok(response);

    }

    @GetMapping("/category")
    public ResponseEntity<BaseApiRespone<List<CategoryRequestModel>>> select() {

        ModelMapper mapper = new ModelMapper();
        BaseApiRespone<List<CategoryRequestModel>> response = new BaseApiRespone<>();

        List<categoryModel> categoryList = categoryImpService.select();
        List<CategoryRequestModel> category = new ArrayList<>();

        for ( categoryModel categorys: categoryList) {
            category.add(mapper.map(categorys, CategoryRequestModel.class));
        }
        response.setMessage("all Categorys successfully");
        response.setData(category);
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
    @DeleteMapping("/category/{id}")
    public ResponseEntity<BaseApiRespone<String>> delete(@PathVariable int id)
    {
        BaseApiRespone<String> response = new BaseApiRespone<>();
        categoryImpService.delete(id);
        response.setMessage("Delete successfully");
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }
    @PutMapping("/category/{id}")
    public ResponseEntity<BaseApiRespone<String>> update(@RequestBody  CategoryRequestModel categoryRequestModel,
                                                         @PathVariable("id") Integer id)
    {
        BaseApiRespone<String> response = new BaseApiRespone<>();
        ModelMapper mapper = new ModelMapper();
        categoryModel cateogry = mapper.map(categoryRequestModel, categoryModel.class);
        categoryImpService.update(cateogry,id);
        response.setMessage("update successfully");
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

}
