package com.example.demo.controller.request;

public class CategoryRequestModel {
    private int id;
    private String name;
    public  CategoryRequestModel(){}

    public CategoryRequestModel(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CategoryRequestModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
