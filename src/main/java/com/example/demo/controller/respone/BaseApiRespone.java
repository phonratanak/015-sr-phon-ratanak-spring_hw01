package com.example.demo.controller.respone;

import org.springframework.http.HttpStatus;

import java.sql.Timestamp;

public class BaseApiRespone<T> {
    private String message;
    private T data;
    private HttpStatus status;
    public BaseApiRespone() {}

    public BaseApiRespone(String message, T data, HttpStatus status, Timestamp time) {
        this.message = message;
        this.data = data;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BaseApiResponse{" +
                "data='" + data + '\'' +
                ",message=" + message+
                ", status=" + status +
                '}';
    }
}
