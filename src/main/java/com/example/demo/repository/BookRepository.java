package com.example.demo.repository;

import com.example.demo.repository.model.BookModel;
import com.example.demo.repository.model.categoryModel;
import com.example.demo.repository.provider.BookProvider;
import com.example.demo.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {
    @Insert("INSERT INTO tb_books (category_id, title, author, description, thumbnail) " +
            "VALUES (#{category_id},#{title},#{author},#{description},#{thumbnail})")
    boolean insert(BookModel bookModel);
    @SelectProvider(value = BookProvider.class, method = "select")
    List<BookModel> select();
    @SelectProvider(value = BookProvider.class, method = "delete")
    void delete(int id);
    @SelectProvider(value = BookProvider.class, method = "findOne")
    BookModel findOne(int id);
    @SelectProvider(value = BookProvider.class, method = "selectByFilter")
    List<BookModel> selectByFilter(String title);
    @Update("UPDATE tb_books SET category_id=#{book.category_id},title=#{book.title},author=#{book.author},description=#{book.description},thumbnail=#{book.thumbnail} WHERE id=#{id}")
    int update(BookModel book,int id);
    @SelectProvider(value = BookProvider.class, method = "selectByFilters")
    List<BookModel> selectByFilters(String title,int category);
}
