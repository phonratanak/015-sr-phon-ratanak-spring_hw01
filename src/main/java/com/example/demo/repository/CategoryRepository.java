package com.example.demo.repository;

import com.example.demo.repository.model.categoryModel;
import com.example.demo.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.SelectProvider;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoryRepository {
    @Insert("INSERT INTO tb_categories (name) " +
            "VALUES (#{name})")
    boolean insert(categoryModel category);

    @SelectProvider(value = CategoryProvider.class, method = "select")
    List<categoryModel> select();
    @SelectProvider(value = CategoryProvider.class, method = "delete")
    void delete(int id);
    @Update("UPDATE tb_categories SET name=#{category.name} WHERE id=#{id}")
    int update(categoryModel category,int id);
}
