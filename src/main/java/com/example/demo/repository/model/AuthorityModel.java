package com.example.demo.repository.model;

public class AuthorityModel {
    private int id;
    private String name;

    public AuthorityModel(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AuthorityModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
