package com.example.demo.repository.model;

public class categoryModel {
    private  int id;
    private String name;
    public categoryModel(){}
    public categoryModel(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "categoryModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
