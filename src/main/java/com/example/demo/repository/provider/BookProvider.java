package com.example.demo.repository.provider;

import org.apache.ibatis.jdbc.SQL;
import org.springframework.stereotype.Repository;

@Repository
public class BookProvider {
    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }
    public String delete(int id) {
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id =#{id}");
        }}.toString();
    }
    public String findOne(int id) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("id =#{id}");
        }}.toString();
    }
    public String selectByFilter(String title) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            if (title != null || !title.equals(""))
                WHERE("title like '%"+title+"%'");
            else
                WHERE();
        }}.toString();
    }
    public String selectByFilters(String title,int category) {
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
            if (title != null || !title.equals(""))
                WHERE("title like '%"+title+"%' AND category_id = "+category);
            else
                WHERE();
        }}.toString();
    }
}
