package com.example.demo.repository.provider;

import com.example.demo.repository.model.categoryModel;
import org.apache.ibatis.jdbc.SQL;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryProvider {
    public String insert()
    {
        return new SQL(){
            {
                INSERT_INTO("tb_categories");
                VALUES("name","#{name}");
            }
        }.toString();
    }
    public String select() {
        return new SQL(){{
            SELECT("*");
            FROM("tb_categories");
        }}.toString();
    }
    public String delete(int id) {
        return new SQL(){{
            DELETE_FROM("tb_categories");
            WHERE("id =#{id}");
        }}.toString();
    }
}
