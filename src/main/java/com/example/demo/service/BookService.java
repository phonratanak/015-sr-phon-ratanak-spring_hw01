package com.example.demo.service;

import com.example.demo.repository.model.BookModel;
import com.example.demo.repository.model.categoryModel;

import java.util.List;

public interface BookService {
    BookModel insert(BookModel bookModel);
    List<BookModel> select();
    void delete(int id);
    BookModel findOne(int id);
    List<BookModel> selectByFilter(String title);
    List<BookModel> selectByFilters(String title,int category);
    int update(BookModel book,int id);
}
