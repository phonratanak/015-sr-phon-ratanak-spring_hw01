package com.example.demo.service;

import com.example.demo.repository.model.categoryModel;

import java.util.List;

public interface CategoryService {
    categoryModel insert(categoryModel category);
    List<categoryModel> select();
    void delete(int id);
    int update(categoryModel category,int id);
}
