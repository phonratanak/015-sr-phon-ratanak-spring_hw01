package com.example.demo.service.bookImpement;

import com.example.demo.repository.BookRepository;
import com.example.demo.repository.model.BookModel;
import com.example.demo.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookImpService implements BookService {
    BookRepository bookRepository;
    @Autowired
    public BookImpService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public BookModel insert(BookModel bookModel) {
        boolean isInserted = bookRepository.insert(bookModel);
        if (isInserted)
            return bookModel;
        else
            return null;
    }

    @Override
    public List<BookModel> select() {
        return bookRepository.select();
    }

    @Override
    public void delete(int id) {
        bookRepository.delete(id);
    }

    @Override
    public BookModel findOne(int id) {
        return bookRepository.findOne(id);
    }

    @Override
    public List<BookModel> selectByFilter(String title) {
        return bookRepository.selectByFilter(title);
    }

    @Override
    public List<BookModel> selectByFilters(String title, int category) {
        return bookRepository.selectByFilters(title,category);
    }

    @Override
    public int update(BookModel book, int id) {
        return bookRepository.update(book,id);
    }
}
