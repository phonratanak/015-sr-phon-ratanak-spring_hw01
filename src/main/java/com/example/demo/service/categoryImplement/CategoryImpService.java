package com.example.demo.service.categoryImplement;

import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.model.categoryModel;
import com.example.demo.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CategoryImpService implements CategoryService {
    private CategoryRepository categoryRepository;
    @Autowired
    public CategoryImpService(CategoryRepository categoryRepository) {

        this.categoryRepository = categoryRepository;
    }

    @Override
    public categoryModel insert(categoryModel category) {
        boolean isInserted = categoryRepository.insert(category);
        if (isInserted)
            return category;
        else
            return null;
    }
    @Override
    public List<categoryModel> select() {
        return categoryRepository.select();
    }

    @Override
    public void delete(int id) {
         categoryRepository.delete(id);
    }

    @Override
    public int update(categoryModel category,int id) {
         return categoryRepository.update(category,id);
    }
}

